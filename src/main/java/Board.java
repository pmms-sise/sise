
import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author Patryk Markowski
 */
public class Board implements Comparable<Board>  {
    //    Board dimensions
    int rows;
    int columns;
    int size;


    //    index of empty element in Array
    int emptyPosition = 0;
    ArrayList<Board> childBoards = new ArrayList<>();
    Board parentBoard;
    int depth;
    char moveSign;
    int[] schema;
    int heur;
    int total;
    public Board (){

    }

    public Board(int[] schema, int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        this.schema = schema;
        this.size = rows * columns;
    }

    public Board(int[] arr) {
        this.rows = arr[0];
        this.columns = arr[1];
        this.size = rows * columns;
        this.schema = Arrays.copyOfRange(arr, 2, arr.length);
    }

    public void Move(int[] currentBoard, int newPosition, int currentPosition, char sign) {
        int[] newBoard = Arrays.copyOf(currentBoard, currentBoard.length);

        int tmp = newBoard[newPosition];
        newBoard[newPosition] = newBoard[currentPosition];
        newBoard[currentPosition] = tmp;

        Board child = new Board(newBoard, columns, rows);
        child.moveSign = sign;
        child.parentBoard = this;
        child.depth = this.depth + 1;
        childBoards.add(child);
    }



    public void moveUp(int[] currentBoard, int position) {
        if (position - columns >= 0) {
            Move(currentBoard, position - columns, position, 'U');
        }
    }

    public void moveDown(int[] currentBoard, int position) {
        if (position + columns < schema.length) {
            Move(currentBoard, position + columns, position, 'D');
        }
    }

    public void moveLeft(int[] currentBoard, int position) {
        if (position % columns > 0) {
            Move(currentBoard, position - 1, position, 'L');
        }
    }

    public void moveRight(int[] currentBoard, int position) {
        if (position % columns < columns - 1) {
            Move(currentBoard, position + 1, position, 'R');
        }
    }

    public void makeChildren(char[] order) {
        emptyPosition = ArrayUtils.indexOf(schema, 0);
        for (int i = 0; i < order.length; i++) {
            switch (order[i]) {
                case 'L':
                    moveLeft(schema, emptyPosition);
                    break;
                case 'R':
                    moveRight(schema, emptyPosition);
                    break;
                case 'U':
                    moveUp(schema, emptyPosition);
                    break;
                case 'D':
                    moveDown(schema, emptyPosition);
                    break;
            }
        }
    }
//zwraca true jeżeli

    public boolean isOrdered() {
        int[] arr = Arrays.stream(schema).limit(size-1).toArray();
        return ArrayUtils.isSorted(arr);
    }


    //zlicza ilość niepoprawnie ustawionych pól + depth
    public void hamming(){
        heur = 0;
        for(int i=0;i<schema.length;i++){
            if(schema[i]!=0 && schema[i]!=i+1){
                heur++;
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Board)) return false;

        Board board = (Board) o;

        return Arrays.equals(schema, board.schema);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(schema);
    }

    @Override
    public int compareTo(Board o) {
        return Integer.compare(this.totalWeight(),o.totalWeight());
    }


    //zlicza dystans (pion i poziom) punktu w którym jest niepoprawnie ustawione pole do pola w którym powinno się znaleźć + depth
    public void manhattan(){
        heur =0;
        for(int i=0;i<schema.length;i++){
            if(schema[i]!=0 && schema[i]!=i+1){
                int correctCol = (schema[i]-1)%columns;
                int correctRow = (schema[i]-1)/rows;
                int incorrectCol = i%columns;
                int incorrectRow = i/rows;
                int distance = Math.abs(correctCol-incorrectCol)+Math.abs(correctRow-incorrectRow);
                heur+=distance;
            }
        }

    }
    public int totalWeight(){
        total = heur+depth;
        return heur+depth;
    }


    public void printBoard() {
        for (int i = 0; i < size; i++) {
            if (i != 0 && i % columns == 0) {
                System.out.println();
            }
            System.out.printf(schema[i] + " ");

        }
        System.out.println();
    }

}
