import java.util.*;

/**
 * @author Patryk Markowski
 */
public class BFS extends Solution {
    public BFS() {
    }

    public Board compute(Board start, char[] letters) {
        Set<Board> visited = new HashSet();
        Queue<Board> openList = new LinkedList<>();

        //target Board
        int[] targetArray = new int[start.size];
        for (int i = 0; i < targetArray.length - 1; i++) {
            targetArray[i] = i + 1;
        }
        Board targetBoard = new Board(targetArray, start.rows, start.columns);

        openList.offer(start);
        while (!openList.isEmpty()) {
            Board peaked = openList.poll();

            if (visited.contains(peaked)) {
                openList.remove(peaked);
                continue;
            }
            if (peaked.depth > maxDepth) {
                maxDepth = peaked.depth;
            }

            if (peaked.equals(targetBoard)) {
                return peaked;
            }

            visited.add(peaked);

            peaked.makeChildren(letters);
//            peaked.makeChildren(new char[]{'L', 'R', 'U', 'D'});

            for (Board child : peaked.childBoards) {
                amountOfProcessedStates++;
                openList.offer(child);
            }
            amountOfVisitedStates++;
        }
        return null;
    }
}
