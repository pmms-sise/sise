
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

/**
 * @author Patryk Markowski
 */
public class FileOperation {
    public int[] readFromFile(String name) throws FileNotFoundException {
        Scanner read = new Scanner(new File(name));
        String lines = "";
        while (read.hasNextLine()) {
            lines += read.nextLine() + " ";
        }
        String[] val = lines.split(" ");
        int[] values = Arrays.asList(val).stream().mapToInt(Integer::parseInt).toArray();
        read.close();
        return values;
    }

    public void writeToFile(String solution, String path) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(path));
        writer.write(solution);

        writer.close();

    }
}
