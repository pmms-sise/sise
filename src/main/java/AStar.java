import java.util.*;

/**
 * @author Patryk Markowski
 */
public class AStar extends Solution{

    public AStar() {
    super();
    }

    public Board compute(Board start, String heuristic) {
        Set<Board> visited = new HashSet();

        PriorityQueue<Board> openList = new PriorityQueue<>(Board::compareTo);

        int[] targetArray = new int[start.size];
        for (int i = 0; i < targetArray.length - 1; i++) {
            targetArray[i] = i + 1;
        }
        Board targetBoard = new Board(targetArray, start.rows, start.columns);
        openList.add(start);

        while (!openList.isEmpty()) {
          Board peaked = openList.peek();

            if (visited.contains(peaked)) {
                openList.remove(peaked);
                continue;
            }
            if(peaked.depth>maxDepth){
                maxDepth= peaked.depth;
            }

            if (peaked.equals(targetBoard)) {
                return peaked;
            }

            openList.remove(peaked);
            visited.add(peaked);

            peaked.makeChildren(new char[]{'L', 'R', 'U', 'D'});

            for (Board child : peaked.childBoards) {
                amountOfProcessedStates++;
                if (heuristic.equals("hamm")) {
                    child.hamming();
                } else {
                    child.manhattan();
                }

                openList.add(child);

            }
            amountOfVisitedStates++;
        }
        return null;
    }

}