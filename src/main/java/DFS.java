import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class DFS extends Solution{
    public DFS() {
    }

    public Board compute(Board start, char[] letters) {
        Set<Board> visited = new HashSet<>();
        Stack<Board> openList = new Stack<>();
        //target Board
        int[] targetArray = new int[start.size];
        for (int i = 0; i < targetArray.length - 1; i++) {
            targetArray[i] = i + 1;
        }
        Board targetBoard = new Board(targetArray, start.rows, start.columns);

        openList.push(start);
        while (!openList.isEmpty()) {
            Board peaked = openList.pop();

//            if (visited.contains(peaked)) {
//                continue;
//            }
            if (peaked.depth > maxDepth) {
                maxDepth = peaked.depth;
            }

            if (peaked.equals(targetBoard)) {
                return peaked;
            }

            visited.add(peaked);
            if (peaked.depth < 20) {
                peaked.makeChildren(letters);
//                peaked.makeChildren(new char[]{'D', 'R', 'L', 'U'});
                for (Board child : peaked.childBoards) {
                    amountOfProcessedStates++;
                    openList.push(child);
                }
            }
            amountOfVisitedStates++;
        }
        return null;
    }
}
