
import java.io.IOException;
import java.util.Objects;


/**
 * @author Patryk Markowski
 */
public class Main {



    public static String lettersToWin(Board e) {
        String letters = e.moveSign + "";
        Board parent = e.parentBoard;
        while (parent != null) {
            if(parent.moveSign=='L' || parent.moveSign=='R' || parent.moveSign=='D'||parent.moveSign=='U') {
                letters += parent.moveSign;
            }
            parent = parent.parentBoard;
        }
        StringBuilder sb = new StringBuilder(letters);
        return sb.reverse().toString();
    }

    public static void main(String[] args) throws IOException {

        /*
        FileOperation fr = new FileOperation();
        int[] a = fr.readFromFile("ukladanka.txt");
        Board board = new Board(a);
        DFS dfs = new DFS();
        Board computed = dfs.compute(board, new char[]{'D', 'R', 'L', 'U'});
        if (Objects.isNull(computed)) {
            System.out.println("nie znalazem");
        }
        else{
            computed.printBoard();
        }

        */

//
//        // testy //
//        AStar aStar = new AStar();
//        String algorithInfo="manh";
//
//        FileOperation fr = new FileOperation();
//        int[] a = fr.readFromFile("ukladanka.txt");
//        Board board = new Board(a);
//       Long start=System.currentTimeMillis();
//       Board result = aStar.compute(board, algorithInfo);
//        Long stop=System.currentTimeMillis();
//        System.out.println("main");
//        System.out.println(lettersToWin(result));
//        System.out.println(lettersToWin(result).length());
//        System.out.println(aStar.amountOfVisitedStates);
//        System.out.println(aStar.amountOfProcessedStates);
//        System.out.println(aStar.maxDepth);
//        System.out.println(stop-start);
//        FileOperation fr = new FileOperation();
//        int[] a = fr.readFromFile("ukladanka.txt");
//        Board board = new Board(a);
//        System.out.println("Początek");
//        board.printBoard();
//        AStar aStar = new AStar();
//        BFS bfs = new BFS();
//        Board computed = aStar.compute(board,"manh");
//
//        DFS dfs = new DFS();
////      Board computed = dfs.compute(board);
////        Board computed = bfs.compute(board);
//        System.out.println("wynik");
//        if(Objects.nonNull(computed)) {
//            computed.printBoard();
//            System.out.println(lettersToWin(computed));
//        }else{
//            System.out.println("nie znalazłem rozwiązania i zwróciłem nulla ");
//        }
//        //////////




        //        wczytanie potrzebnych plików
        if (args.length == 5) {
            //Arguments
            String algorithm = args[0];
            String algorithInfo = args[1]; // LRUD lub dla astar manh albo hamm
            String inputDataPath = args[2];
            String resultDataFilePath = args[3];
            String additionalDataFilePath = args[4];

            FileOperation fr = new FileOperation();
            int[] a = fr.readFromFile(inputDataPath);

            Board board = new Board(a);
            Board result = new Board();

            //additional info
            int amountOfVisitedStates=0;
            int amountOfProcessedStates=0;
            int maxDepth=0;

            //Time measure
            Long start= Long.valueOf("0");
            Long stop= Long.valueOf("0");

            switch (algorithm) {
                case "bfs" -> {
                    BFS bfs = new BFS();
                    start=System.currentTimeMillis();
                    result = bfs.compute(board, algorithInfo.toCharArray());
                    stop=System.currentTimeMillis();
                    amountOfVisitedStates = bfs.amountOfVisitedStates;
                    amountOfProcessedStates= bfs.amountOfProcessedStates;
                    maxDepth=bfs.maxDepth;
                }
                case "dfs" -> {
                    DFS dfs = new DFS();
                    start=System.currentTimeMillis();
                    result = dfs.compute(board, algorithInfo.toCharArray());
                    stop=System.currentTimeMillis();
                    amountOfVisitedStates = dfs.amountOfVisitedStates;
                    amountOfProcessedStates= dfs.amountOfProcessedStates;
                    maxDepth=dfs.maxDepth;
                }
                case "astr" -> {
                    AStar aStar = new AStar();
                    start=System.currentTimeMillis();
                    result = aStar.compute(board, algorithInfo);
                    stop=System.currentTimeMillis();
                    amountOfVisitedStates = aStar.amountOfVisitedStates;
                    amountOfProcessedStates= aStar.amountOfProcessedStates;
                    maxDepth=aStar.maxDepth;
                }
            }

            //Write to file result
            String solution = "";
            int solutionLength = -1;
            if (Objects.nonNull(result)) {
                solution = lettersToWin(result);
                solutionLength = solution.length();
            }

            fr.writeToFile(solutionLength+"\n"+solution, resultDataFilePath);

            //Write to file additional info

            String additionalInfo = solutionLength+"\n"
                    +amountOfVisitedStates+"\n"
                    +amountOfProcessedStates+"\n"
                    +maxDepth+"\n"
                    +(stop-start);
            fr.writeToFile(additionalInfo,additionalDataFilePath);

        }


        //
//        OK 1 linia (liczba całkowita): długość znalezionego rozwiązania - o takiej samej wartości jak w pliku z rozwiązaniem (przy czym gdy program nie znalazł rozwiązania, wartość ta to -1);
//        2 linia (liczba całkowita): liczbę stanów odwiedzonych;
//        3 linia (liczba całkowita): liczbę stanów przetworzonych;
//        4 linia (liczba całkowita): maksymalną osiągniętą głębokość rekursji;
//        5 linia (liczba rzeczywista z dokładnością do 3 miejsc po przecinku): czas trwania procesu obliczeniowego w milisekundach.
//
//
//        } else {
//
//
//        }
////        sprawdzenie czy układanka jest rozwiązywalna

//        jeżeli nie to elo
//        jeżeli jest to odpalamy koparkę bitkoina :D
//        zwracamy



    }
}
